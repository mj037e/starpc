$(document).ready(function(){

	
	$('.tab_tit').on('click',function(){
		var index = $(this).index();
		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		$('.tab_con').not(index).removeClass('active');
		$('.tab_con').eq(index).addClass('active');
		console.log(index);
	});

    lightbox.init();


});
//copy 
function fnCopy(target , memo){
    var copyText = target;
    copyText.select();
    console.log();
    document.execCommand( 'copy' );
    if(copyText.value === '') alert( memo+' 복사를 실패했습니다.')
    else alert(memo+'가 복사 되었습니다.')
}
/*lightbox 2차*/
var lightbox = {//emj 19.04.04 간단한 모달창 띄우기 펑션.
    el : $('[data-lightbox]'),
    elNum : [],
    onEl : '',
    init : function(){
        var that = this;
        $.each(that.el,function(i,v){
            that.elNum[i] = $(v).attr('data-lightbox');
            $(v).on('click',function(){
                that.onEl = that.elNum[i];
                $(that.elNum[i]).removeClass('hidden');
                var contents = $(that.elNum[i]).children('.lightbox-contents').detach();
                var coverInner = $('<div>').attr({'class':'cover-inner'}).append(contents);
                var cover = $('<div>').attr({'class':'cover'}).append(coverInner);
                $(that.elNum[i]).append(cover);
                that.close();
                that.scrollStop();
            });
        });

        $('.lightbox-contents').on('click',function(event){
            that.stopFunc(event);
        });
        $('.lightbox-contents .light-body').on('scroll touchmove mousewheel',function(event){
            that.stopFunc(event);
        });
    },
    stopFunc : function(event){
        event.stopPropagation();
    },
    close : function(){
        var that = this;
        $(that.onEl).find('.close').on('click',function(){
            $(that.onEl).addClass('hidden');
            var contents = $(that.onEl).find('.lightbox-contents').detach();
            $(that.onEl).empty();
            $(that.onEl).append(contents);
            that.onEl = '';
        })
    },
    scrollStop : function(){ //emj 배경에 스크롤 했을떄 스크롤 막기.
        var that = this;
        $(that.onEl).on('scroll touchmove mousewheel',function(event){
        	console.log(1);
            event.preventDefault();
            event.stopPropagation();
            return false;
        })
    },
    scrollOn : function(){//emj 배경에 스크롤 했을떄 스크롤 풀기. 부분적으로 스크롤을 풀고 싶을때 사용.
        var that = this;
        $.each(that.elNum,function(i,v){
            $(v).off('scroll touchmove mousewheel');
        })
    }
}
