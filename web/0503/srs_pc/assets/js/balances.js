$(document).ready(function(){
	
	$('.tab_name').on('click',function(){
		var index = $(this).index();
		$(this).addClass('active').siblings().removeClass('active');
		$('.tab_content').not(':nth-child('+(index+1)+')').addClass('hidden').eq(index).removeClass('hidden');
	});
});

