$(document).ready(function(){

	// /balances menu tab
	$('.tab_tit').on('click',function(){
		var index = $(this).index();
		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		$('.tab_con').not(index).removeClass('active');
		$('.tab_con').eq(index).addClass('active');
		console.log(index);
	});


	/*토큰장부 조회 조회기간 상세설정*/
	if($( ".datepicker" ).length > 0 ){
		$( ".datepicker" ).on({
		// 'show.datepicker': function (e) {
		//   console.log(e.type, e.namespace);
		// },
		// 'hide.datepicker': function (e) {
		//   console.log(e.type, e.namespace);
		// },
		// 'pick.datepicker': function (e) {
		//   console.log(e.type, e.namespace, e.view);
		// }
		}).datepicker({
		show: function (e) {
		  console.log(e.type, e.namespace);
		},
		hide: function (e) {
		  console.log(e.type, e.namespace);
		},
		pick: function (e) {
		  console.log(e.type, e.namespace, e.view);
		},
		daysShort : ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '일요일'],
		daysMin : ['일', '월', '화', '수', '목', '금', '토'],
		months :  ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		monthsShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		format : 'yyyy-mm-dd',
		zIndex : 10,
		offset : 0,
		language : 'ko-KR',
		highlightedClass : 'highlightedClass'
		});
	}


});

function menuOpen(e){
	$('.'+e).show();
}
function menuClose(e){
	$('.'+e).hide();
}

//input copy 
function fnCopy(target , memo){
    var copyText = target;
    copyText.select();
    document.execCommand( 'copy' );
    if(copyText.value === '') swal('알림', memo+' 복사를 실패했습니다.')
    else swal('알림', memo+'가 복사 되었습니다.')
}