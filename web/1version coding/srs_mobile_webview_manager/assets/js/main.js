$(document).ready(function(){
	$('.pc_row .pc').on('click',function(){
		var that = $(this);
		var num = that.children('.pc_num').html();
		var all = that.children('.all_mining').html();
		var user = that.children('.user_mining').html();
		var manager = that.children('.manager_mining').html();

		$('.alert_wrap.pc').addClass('active');
		$('.alert_wrap.pc .num').html(num);
		$('.alert_wrap.pc .all').html(all);
		$('.alert_wrap.pc .user').html(user);
		$('.alert_wrap.pc .manager').html(manager);
	});

	$('.alert_wrap.pc .close').on('click',function(){
		$('.alert_wrap.pc').removeClass('active');
	});

});
